describe "GET /api/clientes/historicoVenda/{cpf}" do
  context "Valida mensagem de retorno" do
    before(:all) do
      cpf = "04384668023"
      @result = Historico.new.create(cpf)
    end

    it "Valida status code com CPF correto" do
      expect(@result.parsed_response["message"]).to eql "Operação realizada com sucesso."
    end
    it "Valida statusCode 200" do
      expect(@result.parsed_response["statusCode"]).to eql 200
    end
  end
  context "Inserindo letras no campo CPF" do
    before(:all) do
      cpf = "0438466jsdjas"
      @resultado = Historico.new.create(cpf)
    end
    # API RETORNANDO CODIGO 200, VALIDAR COM DEV SOBRE ISSO.
    it "Valida mensagem de erro" do
      expect(@resultado.parsed_response["message"]).to eql "Operação realizada com sucesso."
    end
    it "Valida campo data null" do
      expect(@resultado.parsed_response["data"]).to eql []
    end
    it "Valida StatusCode 200" do
      expect(@resultado.parsed_response["statusCode"]).to eql 200
    end
  end
  context "Inserindo mais de 11 digitos" do
    before(:all) do
      cpf = "043846680232323"
      @digitos = Historico.new.create(cpf)
    end
    # API RETORNANDO CODIGO 200, VALIDAR COM DEV SOBRE ISSO.
    it "Valida mensagem de erro" do
      expect(@digitos.parsed_response["message"]).to eql "Operação realizada com sucesso."
    end
    it "Valida campo data null" do
      expect(@digitos.parsed_response["data"]).to eql []
    end
    it "Valida StatusCode 200" do
      expect(@digitos.parsed_response["statusCode"]).to eql 200
    end
  end
  context "Não inserindo CPF" do
    before(:all) do
      cpf = ""
      @sem_cpf = Historico.new.create(cpf)
    end
    it "Valida status not found sem CPF" do
      expect(@sem_cpf.parsed_response["status"]).to eql "NOT_FOUND"
    end

    it "Valida erro 404" do
      expect(@sem_cpf.parsed_response["statusCode"]).to eql 404
    end
    it "Valida data null" do
      expect(@sem_cpf.parsed_response["data"]).to eql nil
    end
  end
end
