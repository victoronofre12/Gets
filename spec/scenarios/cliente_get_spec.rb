describe "GET /api/clientes/{cpf}" do
  context "Valida CPF ok" do
    before(:all) do
      #cpf = { cpf: "04384668023" }
      cpf = "04384668023"
      @result = Cliente.new.create(cpf)
    end

    it "CPF valido" do
      expect(@result.parsed_response["message"]).to eql "Operação realizada com sucesso."
    end
    it "Valida Statuscode" do
      expect(@result.parsed_response["statusCode"]).to eql 200
    end
  end
  context "Valida ENDEREÇO" do
    before(:all) do
      cpf = "04384668023"
      @result = Cliente.new.create(cpf)
    end
    it "Valida RUA" do
      expect(@result["data"]["enderecos"][0]["rua"]).to be_kind_of(String)
      puts "RUA: #{@result["data"]["enderecos"][0]["rua"]}"
    end
    it "Valida ID do endereço" do
      expect(@result["data"]["enderecos"][0]["id"]).to be_kind_of(Integer)
    end
  end

  context "Valida CPF invalido" do
    before(:all) do
      #cpf = { cpf: "04384668023" }
      cpf = "0438466802"
      @resp = Cliente.new.create(cpf)
    end
    it "CPF invalido" do
      expect(@resp.parsed_response["messages"]).to eql []
    end

    it "Valida statusCode 404" do
      expect(@resp.parsed_response["statusCode"]).to eql 404
    end

    it "Valida data Null" do
      expect(@resp.parsed_response["data"]).to eql nil
    end
  end
  context "Login sem CPF" do
    before(:all) do
      #cpf = { cpf: "04384668023" }
      cpf = ""
      @sem_cpf = Cliente.new.create(cpf)
    end

    it "Valida error not found sem CPF" do
      expect(@sem_cpf.parsed_response["error"]).to eql "Not Found"
    end

    it "Valida erro 404" do
      expect(@sem_cpf.parsed_response["status"]).to eql 404
    end
  end
  context "CPF com letras ou caracteres" do
    before(:all) do
      cpf = "043846680av"
      @letra = Cliente.new.create(cpf)
    end
    it " CPF  letra " do
      expect(@letra.parsed_response["message"]).to eql "Cliente não encontrado."
    end
    it "valida status" do
      expect(@letra.parsed_response["status"]).to eql "NOT_FOUND"
    end
    it "valida data null" do
      expect(@letra.parsed_response["data"]).to eql nil
    end
  end
  context " CPF com mais de 11 digitos" do
    before(:all) do
      cpf = "0438466802323"
      @bad = Cliente.new.create(cpf)
    end
    it "CPF com mais de 11 digitos" do
      expect(@bad.parsed_response["status"]).to eql "NOT_FOUND"
    end
    it "valida message" do
      expect(@bad.parsed_response["message"]).to eql "Cliente não encontrado."
    end
    it "Valida statuscode 404" do
      expect(@bad.parsed_response["statusCode"]).to eql 404
    end
  end
end
