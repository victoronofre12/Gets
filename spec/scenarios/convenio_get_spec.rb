describe "GET /api/clientes/convenios/{cpf}" do
  context "valida CPF ok" do
    before(:all) do
      cpf = "04384668023"
      @result = Convenio.new.create(cpf)
    end
    it "CPF Status" do
      expect(@result.parsed_response["status"]).to eql "OK"
    end
    it "valida mensagem de retorno" do
      expect(@result.parsed_response["message"]).to eql "Operação realizada com sucesso."
    end
    it "Valida statuscode" do
      expect(@result.parsed_response["statusCode"]).to eql 200
    end

    it "Valida convenioNome" do
      expect(@result["data"][0]["convenioNome"]).to be_kind_of(String)
      puts "Convenio Nome: #{@result["data"][0]["convenioNome"]}"
    end
    it "Valida convenioId" do
      expect(@result["data"][0]["convenioId"]).to be_kind_of(Integer)
    end
    it "Valida vlrSaldo" do
      expect(@result["data"][0]["vlrSaldo"]).to be_kind_of(Integer)
    end
    it "Valida vlrLimite" do
      expect(@result["data"][0]["vlrLimite"]).to be_kind_of(Integer)
    end
    it "Valida vlrGasto" do
      expect(@result["data"][0]["vlrGasto"]).to be_kind_of(Integer)
    end
    it "Valida vlrGastoFmt" do
      expect(@result["data"][0]["vlrGastoFmt"]).to be_kind_of(String)
    end
    it "Valida vlrLimiteFmt" do
      expect(@result["data"][0]["vlrLimiteFmt"]).to be_kind_of(String)
    end
    it "Valida vlrSaldoFmt" do
      expect(@result["data"][0]["vlrSaldoFmt"]).to be_kind_of(String)
    end
  end
end
