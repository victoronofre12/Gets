require_relative "base_api"

class Cliente < BaseApi
  def create(cpf)
    return self.class.get(
             "/api/clientes/#{cpf}",
             headers: {
               "content-type": "application/json",
             },
           )
  end
end

class Historico < BaseApi
  def create(cpf)
    return self.class.get(
             "/api/clientes/historicoVenda/#{cpf}",
             headers: {
               "content-type": "application/json",
             },
           )
  end
end

class Convenio < BaseApi
  def create(cpf)
    return self.class.get(
             "/api/clientes/convenios/#{cpf}",
             headers: {
               "content-type": "application/json",
             },
           )
  end
end

class SPC < BaseApi
  def create(cpf)
    return self.class.get(
             "/api/clientes/localOrSpc/#{cpf}",
             headers: {
               "content-type": "application/json",
             },
           )
  end
end

class Persiste < BaseApi
  def create(payload)
    return self.class.post(
             "/api/clientes/persistirCliente",
             body: payload.to_json,
             headers: {
               "content-type": "application/json",
             },
           )
  end
end
